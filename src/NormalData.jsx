import React from 'react';
import DeckGL from '@deck.gl/react';
import { ScatterplotLayer } from '@deck.gl/layers';

const viewState = {
	longitude: -122.41669,
	latitude: 37.7853,
	zoom: 12,
	pitch: 0,
	bearing: 0
};

// Generate 1000 sample data points with random coordinates
const generateRandomCoordinates = () => {
	const longitude = -122.4 + Math.random() * 0.575;
	const latitude = 37.75 + Math.random() * 0.315;
	return [longitude, latitude];
};

const data = Array.from({ length: 4000000 }, (_, index) => ({
	name: `Point ${index + 1}`,
	coordinates: generateRandomCoordinates()
}));

function NormalDataComponent() {

	const scatterplotLayer = new ScatterplotLayer({
		id: 'scatterplot-layer',
		data,
		pickable: true,
		opacity: 0.8,
		stroked: true,
		filled: true,
		radiusScale: 10,
		radiusMinPixels: 1,
		radiusMaxPixels: 100,
		lineWidthMinPixels: 1,
		getPosition: d => d.coordinates,
		getRadius: d => Math.sqrt(d.exits),
		getFillColor: d => [255, 140, 0],
		getLineColor: d => [0, 0, 0]
	});

	return (
		<div style={{ width: '100%', height: '600px' }}>
			<DeckGL
				controller={true}
				initialViewState={viewState}
				layers={[scatterplotLayer]}
				getTooltip={({ object }) => object && `${object.name}}`}
			/>
		</div>
	);
}

export default NormalDataComponent;
