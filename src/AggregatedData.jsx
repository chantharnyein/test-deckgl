import React, { useState, useEffect } from 'react';
import DeckGL from '@deck.gl/react';
import { ScatterplotLayer } from '@deck.gl/layers';

const viewState = {
	longitude: -122.41669,
	latitude: 37.7853,
	zoom: 12,
	pitch: 0,
	bearing: 0
};

const generateRandomCoordinates = () => {
	const longitude = -122.4 + Math.random() * 0.575;
	const latitude = 37.75 + Math.random() * 0.315;
	return [longitude, latitude];
};

// Generate 10 million sample data points with random coordinates
const data = Array.from({ length: 4000000 }, (_, index) => ({
	name: `Point ${index + 1}`,
	coordinates: generateRandomCoordinates()
}));

// Aggregating data for improved performance
const aggregatedData = [];
for (let i = 0; i < data.length; i += 100) {
	const group = data.slice(i, i + 100);
	const averageLongitude = group.reduce((sum, point) => sum + point.coordinates[0], 0) / group.length;
	const averageLatitude = group.reduce((sum, point) => sum + point.coordinates[1], 0) / group.length;
	aggregatedData.push({
		name: `Aggregated Group ${i / 100 + 1}`,
		coordinates: [averageLongitude, averageLatitude]
	});
}

function AggregatedDataComponent() {
	const [scatterplotData, setScatterplotData] = useState([]);

	useEffect(() => {
		// Set the aggregated data to state for rendering
		setScatterplotData(aggregatedData);
	}, []);

	const scatterplotLayer = new ScatterplotLayer({
		id: 'scatterplot-layer',
		data: scatterplotData,
		pickable: true,
		opacity: 0.8,
		stroked: true,
		filled: true,
		radiusScale: 10,
		radiusMinPixels: 1,
		radiusMaxPixels: 100,
		lineWidthMinPixels: 1,
		getPosition: d => d.coordinates,
		getRadius: 50, // You can adjust this value based on your preference
		getFillColor: d => [255, 140, 0],
		getLineColor: d => [0, 0, 0]
	});

	return (
		<div style={{ width: '100%', height: '600px' }}>
			<DeckGL
				controller={true}
				initialViewState={viewState}
				layers={[scatterplotLayer]}
				getTooltip={({ object }) => object && `${object.name}`}
			/>
		</div>
	);
}

export default AggregatedDataComponent;
