import React from 'react'
import NormalDataComponent from './NormalData'
import AggregatedDataComponent from './AggregatedData'

function App() {
  return (
	<div>
		<NormalDataComponent />
		{/* <AggregatedDataComponent /> */}
	</div>
  )
}

export default App